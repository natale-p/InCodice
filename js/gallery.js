var extDescDisplayed = false;
var slideCount = 0;
var actualSlide = 0;

function showExtDesc()
{
	$("#container_ext").css("left", (parseInt($("#gallery").width()) + 40) +"px")
	.removeClass('invisible').fadeIn();
	extDescDisplayed = true;
	checkScrollBar();
}

function checkScrollBar() 
{
	var ext_height = $("#container_ext").height();
	if ( $("#gall_ext_desc").attr('scrollHeight') > ext_height) {
		var H = ext_height-10;
		H -= H%7.5;
		$("#gall_ext_desc").height(H+"px").scrollTo('0px');
		$("#container_ext").append('<div id="prevP" style="cursor: pointer;"><<&nbsp;</div>')
		.append('<div id="nextP" style="cursor: pointer;">>></div>');
		$("#nextP").click(function() {
			$("#gall_ext_desc").scrollTo("+=52.5px",800);
		});
		$("#prevP").click(function() {
			$("#gall_ext_desc").scrollTo("-=52.5px",800);
		});		
	} else {
		$("#nextP").remove();
		$("#prevP").remove();
	}
}

function hideExtDesc()
{
	$('#container_ext').fadeOut('fast', function() {
		$("#container_ext").addClass('invisible');
	});
	$("#nextP").remove();
	$("#prevP").remove();
	$("#gall_ext_desc").height('auto');
	extDescDisplayed = false;
}

function pagerClicked(zeroBasedSlideIndex, slideElement)
{
	actualSlide = zeroBasedSlideIndex;
	if (actualSlide == slideCount) {
		$("#gallery").cycle(slideCount-1).stop();
		$("#gallery").children().last().fadeTo('fast', 0.3);
		$("#nav>a").removeClass('activeSlide')
		.last().addClass('activeSlide');
		showExtDesc();
	}
	else if (extDescDisplayed) {
		$("#nav>a").last().removeClass('activeSlide');
		hideExtDesc();
		$("#gallery").stop();
		$("#gallery").children().last().fadeTo('slow', 1.0);
		if (actualSlide == slideCount-1)
			$("#nav>a").eq(slideCount-1).addClass('activeSlide');
	}
	fixArrows();
}

function startGallery()
{
	$('#gallery').cycle({
		pager: "#nav",
		speed: 500,
		onPagerEvent: pagerClicked,
		slideExpr: 'img',
		pagerAnchorBuilder: function(idx, slide) {
			return '<a href="#"><div class="btn"></div></a>';
		},
		height: GHeight,
		width: GWidth,
		fit: 1,
		containerResize: 1,
		fx: 'scrollHorz' // choose your transition type, ex: fade, scrollRight
	});
}

function addLastNav() 
{
	$("#nav").append('<a><div class="btn"></div></a>');
	$("#nav>a").last().click(function() {
		pagerClicked(slideCount, 0);
	});
	$("#gallery").cycle('pause');
}

function fixArrows()
{
	if (actualSlide != 0) {
		$("#prevSlide").removeClass('hidden');
	} else {
		$("#prevSlide").addClass('hidden');
	}
	if (actualSlide != slideCount) {
		$("#nextSlide").removeClass('hidden');
	} else {
		$("#nextSlide").addClass('hidden');
	}
}

function pn_gall(isPrev)
{
	if (isPrev) {
		actualSlide = (actualSlide-1)%(slideCount+1);
	} else {
		actualSlide = (actualSlide+1)%(slideCount+1);
	}

	pagerClicked(actualSlide, null); // fixArrows inside
	if (actualSlide != slideCount)
		$("#gallery").cycle(actualSlide);
}

function makeGallPN()
{
	var toAppend = '<div id="prevSlide" class="invisible hidden">';
	toAppend += '</div><div id="nextSlide" class="invisible hidden"></div>';

	$("#gallery").html(toAppend);
	$("#nav").html("");

	$("#prevSlide").addClass('hidden');
	$("#nextSlide").removeClass('hidden');
}

function showGallery(img, title, desc_it, ext_desc_it, desc_en, ext_desc_en)
{
	if (ext_desc_it == null || ext_desc_it == "")
		ext_desc = "mancante";
	if (desc_it == null || desc_it == "")
		desc_it = "mancante";
	if (desc_en == null || desc_en == "")
		desc_en = "missing";
	if (ext_desc_en == null || ext_desc_en == "")
		ext_desc_en = "missing";

	makeGallPN();
	$("#nextSlide").click(function() { pn_gall(false);});
	$("#prevSlide").click(function() { pn_gall(true); });
	
	actualSlide = 0;
	slideCount = img.length;

	var text = "";
	var info = "width=\""+GWidth+"\" height=\""+GHeight+"\"";
	for (i=0; i<img.length; i++) {
		text += "<img " + info + "src=\""+img[i]+"\">";
	}
	$("#gallery").append(text);

	$("#gall_title_txt").text(title);
	$("#gall_desc_it").text(desc_it);
	$("#gall_desc_en").text(desc_en);

	ext_desc_it = ext_desc_it.replace(/\\n/g, "<br>");
	ext_desc_en = ext_desc_en.replace(/\\n/g, "<br>");

	$("#gall_ext_desc_it").html(ext_desc_it);
	$("#gall_ext_desc_en").html(ext_desc_en);
	pos = "gallery";
	makeGallVis();
	changeGallLang();
	startGallery();
	addLastNav();
}

function makeGallVis()
{
	$("#gallery").removeClass('transparent')
	.stop().fadeTo('slow', 1);
	$("#single, #gall_footer, #gall_title, #gall_title_txt, #gall_description, #gall_footer, #nav").removeClass('invisible');
}

function changeGallLang() {
	if (lang == "ita") {
		$("#gall_desc_en").fadeOut(function() {
			$("#gall_desc_it").removeClass('invisible').fadeIn();
		});
		$("#gall_ext_desc").html($("#gall_ext_desc_it").html());
	} else if (lang == "eng") {
		$("#gall_desc_it").fadeOut(function() {
			$("#gall_desc_en").removeClass('invisible').fadeIn();
		});
		$("#gall_ext_desc").html($("#gall_ext_desc_en").html());
	}
	checkScrollBar();
}

function hideGallery()
{
	$("#gallery").unbind('mouseenter mouseleave');
	$("#gallery").fadeTo('fast', 0).cycle('destroy')
	.removeAttr("display").addClass("transparent").html("");
	$("#single").addClass("invisible");
	hideExtDesc();
	$("#gall_title_txt").text('');
	$("#nav, #gall_title, #gall_title_txt, #gall_description, #it, #en, #gall_desc_it, #gall_desc_en, #gall_ext_desc_it, #gall_ext_desc_en").addClass('invisible');
}
