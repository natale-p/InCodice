var is_work_showed = false;

function prepareWorkTable(div)
{
	if ($.browser.msie) {
		// IE 6-7 with table and not W3C box model
		var toAppend = '<table border="0" align="center" cellpadding="0" cellspacing="0">';
		var divCell = new Array(11).join('<td><div class="cell transparent loading"></div></td>');	
		for (i=0; i<5; i++) {
			toAppend += "<tr>" + divCell + "</tr>";
		}
		toAppend += "</table>";
		$(div).append(toAppend);
	} else {
		var divCell = new Array(11).join('<div class="cell transparent loading"> </div>');
		var toAppend = '';
		for (i=0; i<5; i++) {
			toAppend += '<div class="row"></div>' + divCell;
		}
		$(div).append(toAppend);
	}
}

function parsexmlwork(xml)
{
	prepareWorkTable("#home");
	var i = 0;
	var home_cell = $("#home .cell");
	$(xml).find('block').each(function() {
		var path = $(this).find('path').text();
		var thumb = path+"/"+$(this).find('thumb').attr("name");
		var cell = ((i%5)*10) + parseInt(i/5); i++;
		var loader = $(home_cell).eq(cell);
		var title = $(this).find('title').text();
		var ext_desc_it = $(this).find('extended_it').text();
		var ext_desc_en = $(this).find('extended_en').text();
		var desc_it = $(this).find('description_it').text();
		var desc_en = $(this).find('description_en').text();

		var toAppend = '';
		$(this).find('img').each(function() { 
			toAppend += '<div class="img invisible">'+path+"/"+$(this).attr('name')+'</div>';
		});

		toAppend += '<div class="title invisible">' + title + '</div>';
		toAppend += '<div class="extended_it invisible">'+ext_desc_it+'</div>';
		toAppend += '<div class="extended_en invisible">'+ext_desc_en+'</div>';
		toAppend += '<div class="description_it invisible">'+desc_it+'</div>';
		toAppend += '<div class="description_en invisible">'+desc_en+'</div>';

		$(loader).html(toAppend);

		var img = new Image();
		$(img).load( function() {
			$(this).attr('border', '0').attr('width', cellH).attr('height', cellH).fadeIn();
			$(loader).removeClass('loading').append(this);
		}).attr('src', thumb);
	});
}

function showWork()
{
	hide();
	pos = "work";
	colorMenu(0);
	
	$("#home").removeClass('invisible').stop().fadeTo('slow', 1);

	if (is_work_showed == false) {
		$("#home").addClass('loading');
		$.ajax({
			type: "POST",
			url: "get_img.php",
			data: "dir=img/home",
			async: false,
			dataType: "xml",
			error: function(a,b,c){
				alert('Error loading Work XML document');
				alert(a);
				alert(b);
				alert(c);
			},
			success: parsexmlwork
		});
		is_work_showed = true;

		var home_cell = $("#home .cell");
		$(home_cell).each(function() {
			$(this).fadeTo('slow', 0.5).hover(function() {
				$(this).fadeTo('fast', 1);
			}, function() {
				$(this).fadeTo('fast', 0.5);
			});

			var imgarray = $(this).children(".img");
			var title = $(this).children(".title")[0].innerHTML;
			var description_it = $(this).children(".description_it")[0].innerHTML;
			var description_en = $(this).children(".description_en")[0].innerHTML;
			var ext_desc_it = $(this).children(".extended_it")[0].innerHTML;
			var ext_desc_en = $(this).children(".extended_en")[0].innerHTML;

			if (imgarray.length > 0) {
				var img = new Array();
				for (i=0; i < imgarray.length; i++) {
					var str = imgarray[i].innerHTML;
					var attr = str.split(";");
					img.push(attr[0]);
				}
				$(this).bind('click', function() {
					hideWork();
					$("#gallery").hover(function() { 
						$("#prevSlide").removeClass('invisible');
						$("#nextSlide").removeClass('invisible');
					}, function() {
						$("#prevSlide").addClass('invisible');
						$("#nextSlide").addClass('invisible');
					});
					location.hash = "gallery";
					showGallery(img, title, description_it, ext_desc_it, description_en, ext_desc_en);
				});
			}
		});

		fadeTwo($(home_cell), 35,36);
		$("#home").removeClass('loading');
	}
}

function fadeTwo(container, img1, img2)
{
    var cell1 = $(container).eq(img1);
    var cell2 = $(container).eq(img2);
    
    $(cell1).unbind().hover(function() {
			$(cell1).stop(true,true).fadeTo('fast', 1);
			$(cell2).stop(true,true).fadeTo('fast', 1);
		}, function() {
			$(cell1).stop(true,true).fadeTo('fast', 0.5);
			$(cell2).stop(true,true).fadeTo('fast', 0.5);
	});
	$(cell2).unbind().hover(function() {
			$(cell1).stop(true,true).fadeTo('fast', 1);
			$(cell2).stop(true,true).fadeTo('fast', 1);
		}, function() {
			$(cell1).stop(true,true).fadeTo('fast', 0.5);
			$(cell2).stop(true,true).fadeTo('fast', 0.5);
	});
    
}

function hideWork()
{
	$("#home").fadeTo('fast', 0.0).addClass('invisible');
	$("#single").addClass('invisible');
}
