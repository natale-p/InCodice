function setIta()
{
	lang = "ita";
	if (pos == "gallery")
		changeGallLang();
}

function setEng()
{
	lang = "eng";
	if (pos == "gallery")
		changeGallLang();
}

function showBlank()
{
	hide();
	pos = "blank";

	$("#lnk_about>a").css("color", "#999999");
	$("#lnk_contact>a").css("color", "#999999");
	$("#lnk_work>a").css("color", "#999999");
}

function hideBlank()
{
}
