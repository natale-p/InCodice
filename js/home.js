// JS for home
$(document).ready(start);

var pos = "blank";
var lang = "eng";
var GWidth = null;
var GHeight = null;
var is_about_showed = false;

function getValue(varname)
{
  // First, we load the URL into a variable
  var url = window.location.href;

  // Next, split the url by the ?
  var qparts = url.split("?");

  // Check that there is a querystring, return "" if not
  if (qparts.length == 1)
  {
    return "";
  }
  
  // Then find the querystring, everything after the ?
  var query = qparts[1];

  // Split the query string into variables (separates by &s)
  var vars = query.split("&");

  // Initialize the value with "" as default
  var value = "";

  // Iterate through vars, checking each one for varname
  for (i=0;i<vars.length;i++)
  {
    // Split the variable by =, which splits name and value
    var parts = vars[i].split("=");
    
    // Check if the correct variable
    if (parts[0] == varname)
    {
      // Load value into variable
      value = parts[1];

      // End the loop
      break;
    }
  }
  
  // Convert escape code
  value = unescape(value);

  // Convert "+"s to " "s
  value.replace(/\+/g," ");

  // Return the value
  return value;
}

function colorMenu(active)
{
	var menu = new Array();

	menu.push($("#lnk_work>a"));
	menu.push($("#lnk_about>a"));
	menu.push($("#lnk_contact>a"));

	menu.push($("#lnk_studio>a"));
	menu.push($("#lnk_profile>a"));
	menu.push($("#lnk_exhibition>a"));
	menu.push($("#lnk_press>a"));

	for (i=0; i<menu.length; i++)
		menu[i].css("color", "#999999");

	menu[active].css("color","#6AAAE7");
}

function hide(maintainAboutVisible)
{
	maintainAboutVisible = typeof(maintainAboutVisible) != 'undefined' ? maintainAboutVisible : false;
	if (pos == "gallery") {
		hideGallery();
	} else if (pos == "work") {
		hideWork();
	} else if (pos == "blank") {
		hideBlank();
	} else if (pos == "contact") {
		hideContact();
	} else if (pos == "studio") {
	       hideStudio();
	} else if (pos == "profile") {
		hideBiography();
	} else if (pos == "exhibitions") {
		hideExhibition();
	} else if (pos == "press") {
		hidePress();
	} else {
		if (maintainAboutVisible == true) {
			$("#lnk_studio,#lnk_profile,#lnk_exhibition,#lnk_press").show();
		}
	}
	if (maintainAboutVisible == true) {
		return;
	} else {
		if (pos == "about" || pos == "studio" ||
				pos == "profile" || pos == "exhibitions" ||
			       	pos == "press") {
			hideAbout();
		}
	}
}

function start()
{
	onResize();
	$(window).resize(onResize);

	var lang = getValue("lang");
	if (lang == "it" || lang == "it#") {
		setIta();
	} else {
		setEng();
	}
	showBlank();
	hideAboutMenu();

	$(window).hashchange( function(){
		var hash = location.hash;
		hash = hash.replace( /^#/, '' ) || 'blank';
		if (hash == "work") {
			showWork();
		} else if (hash == "about") {
			showAbout(true);
		} else if (hash == "contact") {
			showContact();
		} else if (hash == "studio") {
			if (is_about_showed == false) showAbout(true);
			showStudio();
		} else if (hash == "profile") {
			if (is_about_showed == false) showAbout(true);
			showProfile();
		} else if (hash == "exhibitions") {
			if (is_about_showed == false) showAbout(true);
			showExhibition();
		} else if (hash == "press") {
			if (is_about_showed == false) showAbout(true);
			showPress();
		} else if (hash == "blank") {
			showBlank();
		}
	});
	showBlank();
	$(window).hashchange();
	first_time = true;
}
