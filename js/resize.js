var cellH;

function resizeLogo(docH)
{
        var logoH = Math.round(docH/6); // orig 6.56

	if (logoH > 100)
		logoH = 100; // Never be >100px

	// If I resize for height, I need to maintain ratio for width
	var lo = $("#logo>a").children('img').eq(0);
	var r = $(lo).width() / $(lo).height();

	$(lo).height(logoH).width(logoH*r);
	$(lo).attr('height', logoH).attr('width', logoH*r);
}

function resizeCell(docH)
{
	var cellH = Math.round(docH / (7.72));
        if (cellH > 90)
                cellH = 90; // Never be >90px

        $(".cell>img").each(function() { $(this).height(cellH).width(cellH); });
	$(".cell>a>img").each(function() { $(this).height(cellH).width(cellH); });

	return cellH;
}

function resizeFont(gallW, gallH)
{
	$("#gall_footer").width(gallW);

	var pallDim = (gallH *0.0158) / 2.67;
	if (pallDim > 3) pallDim=3;

	$("#nav").css("padding-left", pallDim+"px "+pallDim+"px");

	var extDim = gallH / 1.63;
	extDim = extDim.roundTo(0) + 13;

	var ext_t_dim = (extDim - 13) % 7.5;
	$("#gall_ext_desc").height(extDim-ext_t_dim);

	$("#container_ext").height(extDim+"px");
	$("#lang").css("top", (extDim+7.5)+"px");
}

function roundTo(decimalpositions)
{
	var i = this * Math.pow(10,decimalpositions);
	i = Math.round(i);
	return i / Math.pow(10,decimalpositions);
}

Number.prototype.roundTo = roundTo;

function onResize()
{
	var height = $(window).height();
	var width = $(document).width();

	resizeLogo(height);

	cellH = resizeCell(height);

	GHeight = Math.round(cellH * 5);
	GWidth = Math.round(cellH * 7.7);

	if (GHeight > 450) {
		GHeight = 450;
	}
	if (GWidth > 694) {
		GWidth = 694;
	}

	resizeFont(GWidth, GHeight);

	var singleWidth = Math.round(GWidth + 2*cellH);
	$("#single").width(singleWidth).height(GHeight+$("#gall_footer").height());
	$("#studio").width(singleWidth).height(GHeight+$("#gall_footer").height());

	$("#gallery").height(GHeight).width(GWidth);
	$("#gallery>img").height(GHeight).width(GWidth);
	$("#gall_footer").width(GWidth);
	$("#container_ext").css("left", (parseInt($("#gallery").width()) + 15) +"px");
}
