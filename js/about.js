var is_ex_showed = false;
var is_fancy_showed = false;
var is_press_showed = false;

function showProfile()
{
	colorMenu(4);
	$("#lnk_about>a").css("color", "#6AAAE7");
	hide(true);
}

function hideProfile()
{
}

function parsexmlstudio(list)
{
	$("#single").removeClass('invisible');
	$("#gall_title").removeClass('invisible');
	$("#studio").removeClass('invisible').html(list);
	$("#gall_description").removeClass('invisible');
	$("#gall_desc_it").removeClass('invisible').text('COLLABORAZIONI').css("color", "#6AAAE7");
}

function showStudio()
{
	colorMenu(3);
	$("#lnk_about>a").css("color", "#6AAAE7");
	hide(true);
	pos = "studio";
	$.ajax({
		type: "GET",
		url: "collab.html",
		dataType: "html",
		error: function() {
			alert('Error loading Studio XML');
			},
		success: parsexmlstudio
	});
}

function hideStudio()
{	
	$("#gall_description").addClass('invisible');
	$("#gall_desc_it").addClass('invisible').text('').css("color", "#999999");
	$("#studio").addClass('invisible');
	$("#single").addClass('invisible');
}

function showExhibition()
{
	hide(true);
	colorMenu(5);
	$("#lnk_about>a").css("color", "#6AAAE7");
	pos = "exhibitions";
	$("#exh").removeClass("invisible").css('z-index', '4');
	$("#single,#gall_title,#gall_description").removeClass('invisible');
	$("#gall_description").css('font-size', '7pt');
	$("#gall_desc_it").text('').removeClass('invisible');
	if (is_ex_showed) {
		return;
	}
	$("#exh").addClass('loading');
	$.ajax({
                type: "POST",
                url: "get_img.php",
                data: "dir=img/about/exh",
                dataType: "xml",
                error: function() {
                        alert('Error loading Exhibition XML');
                        },
                success: parsexmlexh
        });
	is_ex_showed = true;
}

function parsexmlexh(xml)
{
	$("#exh").removeClass('loading');
	prepareWorkTable("#exh");
	var i = 0;

	var cell_exh = $("#exh .cell");
	$(xml).find('block').each(function() {
		var path=$(this).find('path').text()+"/";
		var thumb=path+$(this).find('thumb').attr('name');
		var title = $(this).find('title').text();
		var info = $(this).find('info').text().replace(/\\n/g, "<br />");
		var img_b = path + $(this).find('big').attr('name');
		var cell = ((i%5)*10)+parseInt(i/5);
		i++;
		var loader = $(cell_exh).eq(cell);
		$(loader).removeClass('transparent');
		if (! thumb.match(/jpg/)) {
			$(loader).removeClass('loading');
		}

		var img = new Image();
		$(img).load(function() {
			$(this).attr('border', '0').attr('width', cellH).attr('height', cellH).attr('title', info);
			$(loader).removeClass('loading').append('<a href="'+img_b+'" title="'+info+'"></a>')
			.children().first().append($(this)).fancybox({
				'autoScale'		: 'false',
				'autoDimensions'	: 'false',
				'hideOnContentClick'	: true,
				'showCloseButton'	: false,
				'padding'		: 0,
				'transitionIn'	:	'elastic',
				'transitionOut'	:	'elastic',
				'onComplete'	:	function() {
							is_fancy_showed = true;
							$("#gall_desc_it").show();
			       				$("#fancybox-wrap").hover(function() {
								$("#fancybox-title").show();
							}, function() {
								$("#fancybox-title").hide();
							});	},
				'onCleanup'	:	function() { $("#gall_desc_it").hide();
			       					is_fancy_showed=false;	},
                                'titlePosition'         : 'over'
                        });
			$(loader).fadeTo('fast', 0.5).hover(function() {
				$(this).fadeTo('fast', 1);
				if (is_fancy_showed == false)
					$("#gall_desc_it").text(title).show();
			}, function() {
				$(this).fadeTo('fast', 0.5);
				if (is_fancy_showed == false)
					$("#gall_desc_it").hide();
			});
		}).attr('src', thumb);
	});

	fadeTwo($(cell_exh), 11, 12);
	fadeTwo($(cell_exh), 36, 37);

}

function hideExhibition()
{
	$("#exh").addClass('invisible').removeAttr('z-index');
	$("#single,#gall_title,#gall_description").addClass('invisible');
	$("#gall_description").css("font-size", "8pt");
	$("#gall_desc_it").show().addClass('invisible');
}

var a_gall_width;

function parsexmlpress(xml)
{
	var text="";
	$(xml).find('block').each(function() {
		var path = $(this).find('path').text();
		$(this).find('img').each(function() {
			text += "<img width=\""+GWidth+"\" height=\""+GHeight+"\" src=\""+path+"/"+$(this).attr('name')+"\"></img>";
		});
	});
	a_gall_width = $("#gallery").width();
	$("#gallery").html(text).css("left", "28%").width(100+"%");
	makeGallVis();
	$("#gall_title_txt").hide();
	$("#nav").html('');
        $('#gallery').cycle({
                pager: "#nav",
                slideExpr: 'img',
                pagerAnchorBuilder: function(idx, slide) {
                        return '<a href="#"><div class="btn"></div></a>';
                },
                height: GHeight,
		fit: 1,
                fx: 'fade' // choose your transition type, ex: fade, scrollRight
        });
	$("#gallery").cycle("pause").removeClass('invisible').fadeIn();
	pos = "press";
}

function showPress()
{
	hide(true);
	colorMenu(6);
	$("#lnk_about>a").css("color", "#6AAAE7");
	pos = "press";

	$.ajax({
		type: "POST",
		url: "get_img.php",
		data: "dir=img/about/press",
		dataType: "xml",
		error: function() {
			alert('Error loading Press XML');
			},
		success: parsexmlpress
	});
}

function hidePress()
{
	$("#gall_title_txt").show();
	hideGallery();
	$("#gallery").css("left", "0px").width(a_gall_width);

}

function showAbout(slide)
{
	hide(true);
	is_about_showed = true;
	var dur = 1000;
	$("#single,#gall_footer,#gall_title").removeClass('invisible');
	
	colorMenu(1);
	pos = "about";

	if (slide != false) {
		$("#lnk_studio,#lnk_profile,#lnk_exhibition,#lnk_press")
			.show("slide", { direction: "left" }, dur);
	}
}

function hideAboutMenu()
{
	$("#lnk_studio,#lnk_profile,#lnk_exhibition,#lnk_press").hide();
	is_about_showed = false;
}

function hideAbout()
{
	hideAboutMenu();
	$("#gall_title,#gall_footer,#single").addClass('invisible');
}
