function showContact()
{
	hide();
	hideAbout();
	colorMenu(2);
	pos = "contact";
	var addr = '';

	addr += 'via dei Rustici, 5<br>';
	addr += '50122 Firenze , Italia<br>';
	addr += '<a href="http://maps.google.it/maps?f=q&source=s_q&hl=it&geocode=&q=via+dei+Rustici,+5+firenze&aq=&sll=41.442726,12.392578&sspn=15.040946,43.286133&ie=UTF8&hq=&hnear=Via+dei+Rustici,+5,+50122+Firenze,+Toscana&ll=43.769839,11.258755&spn=0.007081,0.021136&z=16&iwloc=r3" style="color: #6AAAE7">map</a><br><br>';
	addr += 'tel : +39 055 213770<br>';
	addr += 'mail : <a href="mailto:info@guglielmorenzi.com" style="color: #6AAAE7;">info@guglielmorenzi.com</a><br>';
	
	$("#container_ext").css("left", (parseInt($("#gallery").width()) + 40) +"px");
	$("#gall_ext_desc").html(addr);
	$("#container_ext").removeClass('invisible').removeAttr('display');
	$("#lang").addClass('invisible');
	$("#single").removeClass('invisible');
	$("#contact").removeClass('invisible');
	VideoJS.setupAllWhenReady();
}

function hideContact()
{
	$("#contact").addClass('invisible');
	$("#container_ext").addClass('invisible');
	$("#single").removeAttr("display");
	$("#single").addClass('invisible');
	$("#lang").removeClass('invisible');
	hideExtDesc();
}
