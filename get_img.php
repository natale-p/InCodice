<?php
$dir = $_POST["dir"];
header("Content-type: text/xml; charset=\"UTF-8\"");
$safe_dir = str_replace("/", "-", $dir);
$cachefile = "cache/".$safe_dir.".xml";
if (file_exists($cachefile) && (filemtime($dir)) < filemtime($cachefile)) {
	if ($fp = fopen($cachefile, 'r')) {
		$info = '';
		while (!feof($fp)) {
			$info .= fread($fp, 8192);
		}
		printf($info);
	} else {
		printf("could not read ".$cachefile." but is up-to-date");
	}
} else {
	ob_start();

	printf("<?xml version='1.0' encoding='utf-8'?>\n");
	printf("<list>\n\t");

	$ignore = array( 'cgi-bin', '.', '..'); 
	$dirImgArray = array();

	if ($dh = opendir( $dir )) {
		while( false !== ( $file = readdir( $dh ) ) ){ 
			if( !in_array( $file, $ignore ) ){ 
				if( is_dir( "$dir/$file" ) ){
					$dirImgArray[] = "$dir/$file";
				}
			}
		}
	}
	closedir( $dh ); 
	sort($dirImgArray);

	for ($i = 0; $i<count($dirImgArray); $i++) {
		if ($imgh = opendir($dirImgArray[$i])) {
			$info = "";
			while($entryName = readdir($imgh)) {
				if (preg_match("/xml/i", $entryName)) {
					if ($handle = fopen("$dirImgArray[$i]/$entryName", "r")) {
						while (!feof($handle)) {
							$info .= fread($handle, 8192);
						}
						fclose($handle);
						break;
					}
				}
			}
			printf("<block>\n");
			printf("\t<path>%s</path>\n", $dirImgArray[$i]);
			printf("\t%s\n", $info);
			printf("</block>\n");
		}
		closedir($imgh);
	}
	printf("</list>\n");

	if ($fp = fopen($cachefile, 'w')) {
		fwrite($fp, ob_get_contents());
		fclose($fp);
	} else {
		printf("could not write cache contents to ".$cachefile);
	}
	ob_end_flush();
}

?>
