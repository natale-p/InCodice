<?php
//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
//header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
	<head>
		<title>Guglielmo Renzi e Barbara Fontana Architetture</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-control" content="no-cache">
		<meta http-equiv="Cache-control" content="must-revalidate">
		<meta http-equiv="Cache-control" content="max-age=0">
		<script type="text/javascript" src="js/jquery-1.5.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
		<!--[if lt IE 9]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js">IE7_PNG_SUFFIX=".png";</script>
		<![endif]-->
		<script type="text/javascript" src="js/jquery.fancybox-1.3.4.pack.js"></script>
		<script type="text/javascript" src="js/jquery.easing-1.3.pack.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.5.custom.min.js"></script>
		<script type="text/javascript" src="js/sweet-justice.min.js"></script>

		<script type="text/javascript" src="js/jquery.ba-hashchange.min.js"></script>
		<script type="text/javascript" src="js/video.js"></script>

		<script type="text/javascript" src="js/home.js"></script>
		<script type="text/javascript" src="js/resize.js"></script>
		<script type="text/javascript" src="js/work.js"></script>
		<script type="text/javascript" src="js/about.js"></script>
		<script type="text/javascript" src="js/gallery.js"></script>
		<script type="text/javascript" src="js/blank.js"></script>
		<script type="text/javascript" src="js/contact.js"></script>
<!--[if lte IE 7]>
  <style type="text/css" media="screen">
    .transparent {
      filter: alpha(opacity=0);
    }
  </style>
<![endif]-->
		<link rel="stylesheet" href="css/video-js.css" type="text/css" media="screen" title="Video JS" charset="utf-8">

		<style type="text/css">
			/* Font declaration */
			@font-face {
				font-family: Swiss721ExtBT;
				src: url('fonts/Swiss_721_Extended_BT.ttf');
			}
			@font-face {
				font-family: Swiss721LightCondBT;
				src: url('fonts/Swiss_721_Light_Condensed_BT.ttf');
			}
			/* Some stylish everybody's tearing rules */
			body {
				margin: 0 0 0 0;
				border: none;
				outline: none;
				overflow: hidden;
			}
			a {
				text-decoration:none;
				font-family: Swiss721ExtBT;
				color: #999999;
				outline: none;
			}
			/* The logo. Which isn't touchable. */
			#logo {
				top: 10px;
				left: 24%;
				position: absolute;
				z-index: 5;
			}
			/* class used in work page */
			.table {
				position: absolute;
				left: 9%;
				top: 23%;
				overflow: hidden;
				display: table;
			}
			.row {
				display: table-row;
			}
			.cell {
				display: table-cell;
			}
			.loading {
				background: url(img/progress.gif) no-repeat center;
			}
			.transparent {
				opacity: 0.0;
			}
			.hidden {
				visibility: hidden;
			}
			.invisible {
				visibility: hidden;
				display: none;
			}
			/* All others */
			#menu, #single, #contact, #studio {
				left: 7%;
				font-family: Swiss721ExtBT;
				color: #999999;
				position: absolute;
			}
			#menu {
				font-size: 7pt;
				top: 16%;
				z-index: 2;
			}
			#lnk_about {
				float: left;
			}
			#lnk_studio {
				font-size: 7pt;
				display: inline;
				float: left;
			}
			#lnk_profile {
				font-size: 7pt;
				display: inline;
				float: left;
			}
			#lnk_exhibition {
				font-size: 7pt;
				display: inline;
				float: left;
			}
			#lnk_press {
				font-size: 7pt;
				display: inline;
				float: left;
			}
			#lnk_contact {
				display: block;
				clear: both;
			}
			#single, #studio {
				margin-top: 5px;
				top: 23%;
				width: 785px;
				height: 475px;
				position: absolute;
			}
			#gallery {
				overflow: hidden;
				white-space:nowrap;
				padding-bottom: 14px;
			}
			#contact {
				margin-top: 5px;
				top: 23%;
				left: 24%;
				width: 360px;
				height: 270px;
				position: absolute;
			}
			#studio {
				left: 24%;
				position: absolute;
				line-height: 1.8;
				z-index: 5;
			}
			#studio a {
				color: #3f3f3f;
			}
			#gall_footer {
				font-size: 8pt;
				display: inline-block;
			}
			#gall_title {
				float: left;
				display: inline;
			}
			#gall_description {
				display: inline;
				float: right;
				padding-right: 1px;
			}
			#nav {
				display: inline;
				position: absolute;
				padding-left: 40px;
				padding-top: 3px;
				width: 38%;
			}
			#nav a {
				margin: 0 3px;
				padding: 3px 3px;
				border: 1px solid #ccc;
				background-color: white;
				display: block;
				text-decoration: none;
				float: left;
				cursor: pointer;
			}
			.btn {
				width: 100%;
				height: 100%;
				display: block;
			}
			#nav a.activeSlide {
				background: #666666
			}
			#nav a:focus {
				outline: none;
			}
			#gall_ext_cont {
				color: #333333;
				overflow: hidden;
			}
			#gall_ext_desc {
				overflow: hidden;
				width: 93%;
			}
			#prevSlide, #nextSlide {
				top: 50%;
				background-repeat: no-repeat;
				width: 20px;
				height: 25px;
				z-index: 30;
				position: relative;
				float: left;
				cursor: pointer;
			}
			#prevSlide {
				left: 0%;
				background-image: url(img/scroll-left.gif);
			}
			#nextSlide {
				left: 95%;
				background-image: url(img/scroll-right.gif);
			}
			#lang {
				float: right;
			}
			#container_ext {
				top: 0px;
				font-size: 7.5pt;
				position: absolute;
				width: 27%;
				overflow: hidden;
			}
			#prevP, #nextP {
				float: left;
			}
			.s_name {
				color: #999999;
				font-family: Helvetica;
				font-size: 10pt;
				float: left;
				cursor: default;
			}
			.s_name_spec {
				color: #3f3f3f;
				cursor: default;
			}
			#studio a {
				font-family: Helvetica;
			}
		</style>
	</head>

	<body>
		<div id="logo">
			<a href="#blank">
				<img src="img/logo.png" border="0"/>
			</a>
		</div>
		<div id="menu">
			<div id="lnk_work">
				<a href="#work">WORK</a>
			</div>
			<div id="lnk_about">
				<a href="#about">ABOUT</a>
				&nbsp;
			</div>
			<div id="lnk_contact">
				<a href="#contact">CONTACT</a>
			</div>
		</div>
		<div id="home" class="table invisible"></div>
		<div id="exh" class="table invisible"></div>
		<div id="contact" class="invisible video-js-box">
    <!-- Using the Video for Everybody Embed Code http://camendesign.com/code/video_for_everybody -->
    <video class="video-js" width="360px" height="270px" controls preload poster="img/contact/first.png">
      <source src="video/contact/sito.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <source src="video/contact/sito.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="video/contact/sito.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object class="vjs-flash-fallback" width="360px" height="270px" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["http://guglielmorenzi.com/nat/home/img/contact/first.png", {"url": "http://guglielmorenzi.com/nat/home/video/contact/sito.mp4","autoPlay":false,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="img/contact/first.png" width="360px" height="270px" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
    </video>
	</div>
	<div id="studio" class="sweet-justice invisible"></div>
		<div id="single" class="invisible">
			<div id="gallery" class="transparent">
				<div id="prevSlide" class="invisible"></div>
				<div id="nextSlide" class="invisible"></div>
			</div>
			<div id="container_ext" class="invisible">
				<div id="gall_ext_cont">
					<div id="gall_ext_desc" class="sweet-justice"></div>
					<div id="gall_ext_desc_it" class="invisible"></div>
					<div id="gall_ext_desc_en" class="invisible"></div>
				</div>
				<div id="lang">
					<a href="javascript:setIta()">IT</a> | <a href="javascript:setEng()">EN</a>
				</div>
			</div>
			<div id="gall_footer" class="invisible">
				<div id="gall_title" class="invisible">
					<div id="gall_title_txt" class="invisible"></div>
					<div id="lnk_studio">
						<a href="#studio">STUDIO</a>
						&nbsp;
					</div>
					<div id="lnk_profile">
						<a href="#profile">PROFILE</a>
						&nbsp;
					</div>
					<div id="lnk_exhibition">
						<a href="#exhibitions">EXHIBITIONS</a>
						&nbsp;
					</div>
					<div id="lnk_press">
						<a href="#press">PRESS</a>
						&nbsp;
					</div>
				</div>
				<div id="gall_description" class="invisible">
					<div id="gall_desc_it" class="invisible"></div>
					<div id="gall_desc_en" class="invisible"></div>
				</div>
			</div>
			<div id="nav" class="invisible"></div>
		</div>

		<script type="text/javascript">

			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-17329281-1']);
			_gaq.push(['_trackPageview']);

			(function() {
			 //    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			 //    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			 //    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			 })();

		 </script>
	 </body>
 </html>
